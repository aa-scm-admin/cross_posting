#!/usr/bin/python

import MySQLdb
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

db = MySQLdb.connect(host="127.0.0.1",port=3306,user="root",db="cross_posts")
db.autocommit(True)
cursor = db.cursor()

sql="insert into crosstest_matchstr values (%s,'%s')"
count = 0

f = open("match_strings.tsv","r")

for line in f:
  vals = line.split("\t")
  eid  = vals[0].strip()
  strings = vals[1].strip().split(",")
  count += len(strings)
  #print "eid : %s ; strings : %s" %(eid,strings)

  try:
     for s in strings:
        new_str = s.replace("'","''").replace("\\","\\\\").encode('utf-8')
        cursor.execute("%s" %(sql %(eid,new_str)))
        db.commit()
  except Exception,e:
     print "error is : %s" %(str(e))
     print "s : %s" %(s)

f.close()

print "expected no. of records : %s" %(count)






